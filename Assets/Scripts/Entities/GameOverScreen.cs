﻿using BallonGame.Controllers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BallonGame.Entities
{
    public class GameOverScreen : MonoBehaviour
    {
        [SerializeField] private Text _lvlScore;
        [SerializeField] private Button _buttonNext;
        [SerializeField] private GameObject _gameOverContent;

        private int _score;

        public static UnityAction ShowLvlScoreAction;

        public void ShowGameOverContent(int score)
        {
            _score = score;
            _gameOverContent.SetActive(true);
            _lvlScore.gameObject.SetActive(false);
        }

        private void Awake() => Init();

        private void Init()
        {
            _gameOverContent.SetActive(false);
            _lvlScore.gameObject.SetActive(false);
            _buttonNext.onClick.AddListener(() =>
            {
                GameController.ResumeGameAction();
                _gameOverContent.SetActive(false);
                _lvlScore.gameObject.SetActive(false);
            });
        }

        private void ShowLvlScore()
        {
            _lvlScore.gameObject.SetActive(true);
            _lvlScore.text = $"+{_score}";
        }

        private void OnEnable() => ShowLvlScoreAction += ShowLvlScore;

        private void OnDisable() => ShowLvlScoreAction -= ShowLvlScore;
    }
}
