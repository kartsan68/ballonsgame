﻿using UnityEngine;

namespace BallonGame.Entities
{
    public class Clap : MonoBehaviour
    {
        //Вызывается из анимации, уничтожение объекта 
        public void DestroyThis() => Destroy(gameObject);
    }
}
