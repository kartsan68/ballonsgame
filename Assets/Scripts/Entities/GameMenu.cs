﻿using BallonGame.Controllers;
using UnityEngine;
using UnityEngine.UI;

namespace BallonGame.Entities
{
    public class GameMenu : MonoBehaviour
    {
        [SerializeField] private GameObject _menuContent;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _resumeButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _backToHome;

        private void Awake()
        {
            _menuContent.SetActive(true);
            _resumeButton.gameObject.SetActive(false);

            AddListenersToButtons();
        }

        private void PauseGame()
        {
            GameController.PauseGameAction();
            _menuContent.SetActive(true);
            _playButton.gameObject.SetActive(false);
            _resumeButton.gameObject.SetActive(true);
        }

        private void StartGame()
        {
            GameController.StartGameAction();
            _menuContent.SetActive(false);
        }

        private void ResumeGame()
        {
            GameController.ResumeGameAction();
            _menuContent.SetActive(false);
        }

        private void AddListenersToButtons()
        {
            _backToHome.onClick.AddListener(() => PauseGame());
            _playButton.onClick.AddListener(() => StartGame());
            _resumeButton.onClick.AddListener(() => ResumeGame());
            _exitButton.onClick.AddListener(() => GameController.ExitGameAction());
        }

    }
}
