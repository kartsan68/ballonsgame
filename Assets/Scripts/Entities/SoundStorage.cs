﻿using BallonGame.Data;
using UnityEngine;

public class SoundStorage : MonoBehaviour
{
    [SerializeField] private BallonSound[] _ballonSounds;
    [SerializeField] private Sound[] _gameSounds;

    //Возвращает звук контента шара по типу его контента
    public BallonSound GetSoundByType(BallonContentType contentType)
    {
        foreach (var ballonSound in _ballonSounds)
        {
            if (ballonSound.type == contentType) return ballonSound;
        }

        return new BallonSound();
    }

    //Возвращает игровой звук по типу
    public Sound GetSoundByType(SoundType contentType)
    {
        foreach (var gameSound in _gameSounds)
        {
            if (gameSound.type == contentType) return gameSound;
        }

        return new Sound();
    }

    //Возвращает звуки всех шаров
    public BallonSound[] GetAllBallonSounds() => _ballonSounds;

    //Возвращает все игровые звуки
    public Sound[] GetAllGameSounds() => _gameSounds;

}
