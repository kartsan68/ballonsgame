﻿using UnityEngine;

namespace BallonGame.Entities
{
    public class Star : MonoBehaviour
    {
        //Вызывается из анимации звезды, показывает, сколько очков игрок заработал за уровень
        public void ShowScore() => GameOverScreen.ShowLvlScoreAction();
    }
}
