﻿using System.Collections;
using System.Collections.Generic;
using BallonGame.Data;
using UnityEngine;

namespace BallonGame.Entities
{
    public class BallonsSpawner : MonoBehaviour
    {
        [SerializeField] private Boundary _deltaSpawnBoundary;
        [SerializeField] private GameContentStorage _contentStorage;
        [SerializeField] private Ballon _ballon;
        [SerializeField] private RectTransform _spawnParentRectTransform;
        [SerializeField] private RectTransform _gameCanvasRectTransform;
        [SerializeField] private Range _ballonMoveUpDurationRange;

        private Boundary _spawnBoundary;
        private float _spawnZoneLenght;
        private int _ballonCount;
        private List<BallonContent> _waveBallonContent;

        public void WaveSpawn(Wave wave)
        {
            _ballonCount = wave.waveContent.Length;
            CreateWaveBallonContent(wave);
            StartCoroutine(WaveSpawnStart());
        }

        private void Start() => CalcSpawnBoundary();

        //Рассчет зоны в которой будут появляться шары
        private void CalcSpawnBoundary()
        {

            Vector2 minBoundary = new Vector2(-_gameCanvasRectTransform.rect.width / 2, -_gameCanvasRectTransform.rect.height / 2);
            Vector2 maxBoundary = new Vector2(_gameCanvasRectTransform.rect.width / 2, 0);

            _spawnBoundary = new Boundary()
            {
                minBoundary = minBoundary + _deltaSpawnBoundary.minBoundary,
                maxBoundary = maxBoundary + _deltaSpawnBoundary.maxBoundary
            };

            _spawnZoneLenght = (Mathf.Abs(_spawnBoundary.maxBoundary.x) + Mathf.Abs(_spawnBoundary.minBoundary.x));
        }

        //Заполнение шаров контентом 
        private void CreateWaveBallonContent(Wave wave)
        {
            BallonContent saveBallonContent;
            _waveBallonContent = new List<BallonContent>();

            for (int i = 0; i < _ballonCount; i++)
            {
                for (int j = 0; j < _contentStorage.GetBallonContents.Length; j++)
                {
                    saveBallonContent = _contentStorage.GetBallonContents[j];
                    if (wave.waveContent[i] == saveBallonContent.contentType)
                    {
                        _waveBallonContent.Add(saveBallonContent);
                        break;
                    }
                }
            }
        }

        //Спавн шаров
        private IEnumerator WaveSpawnStart()
        {
            float xStep = _spawnZoneLenght / _ballonCount;
            int ballonCountIterator = _ballonCount;
            int randomContentIndex;
            float yStopPosition;
            float ballonMoveUpDuration;

            Vector2 spawnPosition = new Vector2(_spawnBoundary.minBoundary.x + xStep / 2, _spawnBoundary.minBoundary.y);

            while (ballonCountIterator > 0)
            {
                ballonCountIterator--;

                yield return null;

                Ballon spawnBallon = Instantiate(_ballon, spawnPosition, Quaternion.identity);
                spawnBallon.transform.SetParent(_spawnParentRectTransform, false);

                randomContentIndex = Random.Range(0, _waveBallonContent.Count);
                BallonContent actualContent = _waveBallonContent[randomContentIndex];
                _waveBallonContent.RemoveAt(randomContentIndex);

                yStopPosition = (_gameCanvasRectTransform.rect.height / 2);
                ballonMoveUpDuration = Random.Range(_ballonMoveUpDurationRange.min, _ballonMoveUpDurationRange.max);
                spawnBallon.Injection(actualContent, ballonMoveUpDuration, yStopPosition);

                spawnPosition = new Vector2(spawnPosition.x + xStep, _spawnBoundary.minBoundary.y);
            }
        }

    }
}
