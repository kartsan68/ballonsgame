﻿using BallonGame.Data;
using UnityEngine;

namespace BallonGame.Entities
{
    public class GameContentStorage : MonoBehaviour
    {
        [SerializeField] private BallonContent[] _ballonsContent;

        public BallonContent[] GetBallonContents => _ballonsContent;
    }
}

