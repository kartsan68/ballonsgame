﻿using BallonGame.Data;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using BallonGame.Controllers;
using UnityEngine.Events;

namespace BallonGame.Entities
{
    public class Ballon : MonoBehaviour
    {
        [SerializeField] private GameObject clap;
        [SerializeField] private Vector3 _explodeMovePosition;
        [SerializeField] private TapAnimationData _tapAnimationData;
        [SerializeField] private Vector2 _ballonSizeDivider;
        [SerializeField] private Image _ballonImage;
        [SerializeField] private Image _onBallonImage;

        private RectTransform _rectTransform;
        private BallonContent _content;
        private float _yStopPosition;
        private float _moveSpeed;

        public static UnityAction<Vector3> ExplosionAction;
        public static UnityAction<BallonContentType, bool> BallonTouchResponseAction;

        public void Injection(BallonContent ballonContent, float moveSpeed, float yStopPosition)
        {
            SetBallonContent(ballonContent);

            _moveSpeed = moveSpeed;
            _yStopPosition = (yStopPosition - _rectTransform.rect.height / 2);
            Move();
        }

        private void Awake() => Init();

        //Инициальзация переменных, которые этого требуют
        private void Init()
        {
            _rectTransform = GetComponent<RectTransform>();
            if (_ballonSizeDivider.x == 0 || _ballonSizeDivider.y == 0)
            {
                Debug.Log("SIZE DIVIDER == 0!");
                _ballonSizeDivider = new Vector2(1, 1);
            }
        }

        //Движение вверх
        private void Move() => _rectTransform.DOLocalMoveY(_yStopPosition, _moveSpeed);

        //Нажатие на шар, вызывается событием из PlayerController
        private void Click()
        {
            GameController.BallonTouchAction(_content.contentType);
            ClickReaction();
        }

        //Реакция на нажатие на шар
        private void ClickReaction()
        {
            var seq = DOTween.Sequence();
            seq.Append(_rectTransform.DOBlendableScaleBy(new Vector3(-_tapAnimationData.scaleX, -_tapAnimationData.scaleY, 0), _tapAnimationData.pressInterval));
            seq.AppendInterval(_tapAnimationData.pressInterval);
            seq.Append(_rectTransform.DOBlendableScaleBy(new Vector3(_tapAnimationData.scaleX, _tapAnimationData.scaleY, 0),
             _tapAnimationData.backInterval).OnComplete(() => seq.Kill()));
        }

        //Реакция на взрыв соседнего шара
        private void ExplodeReaction(Vector3 position)
        {
            var heading = position - _rectTransform.localPosition;
            Vector3 moveVector;

            if (heading.x > 0)
            {
                moveVector = new Vector3(-_explodeMovePosition.x, _explodeMovePosition.y);
                _rectTransform.DOLocalMove(moveVector, 2).OnComplete(() => CompleteMoving());
            }
            else
            {
                moveVector = new Vector3(_explodeMovePosition.x, _explodeMovePosition.y);
                _rectTransform.DOLocalMove(moveVector, 2).OnComplete(() => CompleteMoving());
            }
        }

        private void CompleteMoving() => Destroy(gameObject);

        //Реакция на ответ о правильном/не правильном выборе игрока
        private void TouchResponse(BallonContentType contentType, bool isCorrectly)
        {
            if (isCorrectly && contentType == _content.contentType)
            {
                Instantiate(clap, transform.position, Quaternion.identity, transform.parent);
                Destroy(gameObject);
                ExplosionAction(_rectTransform.localPosition);
            }
        }

        //Установка контента шара
        private void SetBallonContent(BallonContent newContent)
        {
            _content = newContent;

            Sprite onBallonSprite = newContent.spriteOnBallon;
            Sprite ballonSprite = newContent.ballonSprites[Random.Range(0, newContent.ballonSprites.Length)];
            _ballonImage.sprite = ballonSprite;
            SetElementSize(ballonSprite);

            if (onBallonSprite != null)
            {
                _onBallonImage.gameObject.SetActive(true);
                _onBallonImage.sprite = onBallonSprite;
            }
            else _onBallonImage.gameObject.SetActive(false);
        }

        //Рассчет и установка размера шарика
        private void SetElementSize(Sprite sprite)
        {
            Vector2 spriteSize = sprite.rect.size;
            _rectTransform.sizeDelta = (spriteSize / _ballonSizeDivider);
        }

        private void OnEnable()
        {
            BallonTouchResponseAction += TouchResponse;
            ExplosionAction += ExplodeReaction;
        }

        private void OnDisable()
        {
            BallonTouchResponseAction -= TouchResponse;
            ExplosionAction -= ExplodeReaction;
        }
    }
}
