﻿using UnityEngine;
using UnityEngine.UI;

namespace BallonGame.Controllers
{
    public class ProgressController : MonoBehaviour
    {
        [SerializeField] private Text _scoreText;
        [SerializeField] private Slider _lvlProgressSlider;

        //Установка полосы прогресса в стартовое состояние
        public void NewWaveDataSet(int maxCountProgress)
        {
            _lvlProgressSlider.minValue = 0;
            _lvlProgressSlider.maxValue = maxCountProgress;
            _lvlProgressSlider.value = 0;
        }

        //Инкрементирование очков отображающихся пользователю в виде текста и прогресс бара
        public void TrueChoice(int score)
        {
            _lvlProgressSlider.value++;
            _scoreText.text = score.ToString();
        }
    }
}