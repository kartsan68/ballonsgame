﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BallonGame.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Camera _camera;
        [SerializeField] private GraphicRaycaster _raycaster;
        [SerializeField] private EventSystem _eventSystem;

        private PointerEventData _pointerEventData;
        private List<RaycastResult> _results;

        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0)) Click(Input.mousePosition);
#else
            if (Input.touchCount > 0)
            {
                if (Input.touches[0].phase == TouchPhase.Began)
                    Click(Input.touches[0].position);
            }
#endif
        }

        private void Click(Vector2 clickPos)
        {
            InitAllClickData(clickPos);

            _raycaster.Raycast(_pointerEventData, _results);
            //Проверяется первый нажатый элемент, нет необходимости проверять весь лист результатов _results
            if (_results.Count > 0 && _results[0].gameObject.tag.Equals("Ballon"))
                _results[0].gameObject.SendMessage("Click");
        }

        private void InitAllClickData(Vector2 clickPosition)
        {
            _pointerEventData = new PointerEventData(_eventSystem);
            _results = new List<RaycastResult>();
            _pointerEventData.position = clickPosition;
        }
    }
}
