﻿using System.Collections.Generic;
using BallonGame.Data;
using UnityEngine;
using UnityEngine.Events;

namespace BallonGame.Controllers
{
    public class SoundController : MonoBehaviour
    {
        [SerializeField] private SoundStorage _soundStorage;
        [SerializeField, Range(0, 1)] private float _oneShotSoundsVolume;
        [SerializeField, Range(0, 1)] private float _loopSoundsVolume;

        private AudioSource _onePlayAudioSource;
        private Dictionary<SoundType, AudioSource> _loopAudioSources;

        public static UnityAction<BallonContentType> PlayBallonColorSoundAction;
        public static UnityAction<SoundType, bool> PlayGameSoundAction;
        public static UnityAction<SoundType, bool> MuteSoundAction;

        private void Awake() => Init();

        private void Init() => _loopAudioSources = new Dictionary<SoundType, AudioSource>();

        //Произнести название контента шара
        private void PlayBallonCollorSound(BallonContentType type) => PlaySoundOnce(_soundStorage.GetSoundByType(type).audioClip);

        //Воспроизвести звуковой эффект
        private void PlayGameSound(SoundType type, bool loop)
        {
            if (!loop) PlaySoundOnce(_soundStorage.GetSoundByType(type).audioClip);
            else PlaySoundLoop(type, _soundStorage.GetSoundByType(type).audioClip);
        }

        //Воспроизвести audioClip при помощи метода PlayOneShot
        private void PlaySoundOnce(AudioClip audioClip)
        {
            GameObject _oneShotSoundObject;

            if (_onePlayAudioSource == null)
            {
                _oneShotSoundObject = new GameObject("OnePlaySound");
                _oneShotSoundObject.AddComponent<AudioSource>();
            }
            else _oneShotSoundObject = _onePlayAudioSource.gameObject;

            _onePlayAudioSource = _oneShotSoundObject.GetComponent<AudioSource>();
            _onePlayAudioSource.volume = _oneShotSoundsVolume;
            _onePlayAudioSource.clip = audioClip;
            _onePlayAudioSource.Play();
        }

        //Воспроизвести audioClip и зациклить 
        private void PlaySoundLoop(SoundType type, AudioClip audioClip)
        {
            AudioSource audioSource;

            if (!AlredyLiveObject(type))
            {
                GameObject _loopAudioObject = new GameObject(type + "Sound");
                audioSource = _loopAudioObject.AddComponent<AudioSource>();
                _loopAudioSources.Add(type, audioSource);
            }
            else audioSource = _loopAudioSources[type];

            audioSource.volume = _loopSoundsVolume;
            audioSource.loop = true;
            audioSource.clip = audioClip;
            audioSource.Play();
        }

        //Заглушить или вернуть требуемый звук
        private void MuteSound(SoundType type, bool isMute)
        {
            if (AlredyLiveObject(type))
            {
                if (isMute) _loopAudioSources[type].volume = 0;
                else _loopAudioSources[type].volume = _loopSoundsVolume;
            }
        }

        //Проверка на наличие уже созданного AudioSource
        private bool AlredyLiveObject(SoundType type)
        {
            foreach (var obj in _loopAudioSources)
                if (obj.Key == type) return true;

            return false;
        }

        private void OnEnable()
        {
            PlayBallonColorSoundAction += PlayBallonCollorSound;
            PlayGameSoundAction += PlayGameSound;
            MuteSoundAction += MuteSound;
        }

        private void OnDisable()
        {
            PlayBallonColorSoundAction -= PlayBallonCollorSound;
            PlayGameSoundAction -= PlayGameSound;
            MuteSoundAction -= MuteSound;
        }
    }
}
