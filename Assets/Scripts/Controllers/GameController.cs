﻿using System.Collections;
using BallonGame.Data;
using BallonGame.Entities;
using UnityEngine;
using UnityEngine.Events;

namespace BallonGame.Controllers
{
    public class GameController : MonoBehaviour
    {
        //выбор волн созданных в ручную или сгенерированных кодом
        [SerializeField] private LevelsType _levelsType;
        [SerializeField] private float _waveDelay;
        [SerializeField] private LevelGenerator _levelGenerator;
        [SerializeField] private BallonsSpawner _ballonsSpawner;
        [SerializeField] private ProgressController _progressController;
        [SerializeField] private GameOverScreen _gameOverScreen;

        private int _actualLvl = 0;
        private int _actualWaveIndex = 0;
        private int _score = 0;
        private int _lvlScore = 0;
        private bool _badChoice = false;
        private bool _gameStop = false;
        private BallonContentType _trueContentType;

        public static UnityAction StartGameAction;
        public static UnityAction ResumeGameAction;
        public static UnityAction ExitGameAction;
        public static UnityAction PauseGameAction;
        public static UnityAction<BallonContentType> BallonTouchAction;

        public void GameStart()
        {
            PlayGameSound(Data.SoundType.GameBackgroundSound, true);

            _lvlScore = 0;
            _progressController.NewWaveDataSet(_levelGenerator.GetLevels(_levelsType)[_actualLvl].redyLevelScores);

            StartCoroutine(WaveSpawn(true));
        }

        //Вызывается, когда игрок нажал на какой-либо из шаров, проверяет верность выбора
        private void PlayerChangeBallon(BallonContentType changedContentType)
        {
            bool changeResult = (_trueContentType == changedContentType ? true : false);

            Ballon.BallonTouchResponseAction(changedContentType, changeResult);

            if (changeResult)
            {
                PlayGameSound(SoundType.BallonExplode, false);

                if (!_badChoice)
                {
                    _lvlScore++;
                    _score++;
                    _progressController.TrueChoice(_score);
                }
                StartCoroutine(WaveSpawn());
            }
            else
            {
                _badChoice = true;
                ToVoiceContent();
            }
        }

        //Заспавнить следующую волну, или первую на старте, при условии что firstWave = true
        private IEnumerator WaveSpawn(bool firstWave = false)
        {
            if (!firstWave)
            {
                yield return new WaitForSeconds(_waveDelay);
                _actualWaveIndex++;
            }

            if (_levelGenerator.GetLevels(_levelsType)[_actualLvl].waves.Length <= _actualWaveIndex) _actualWaveIndex = 0;

            CheckLvl();

            while (_gameStop) yield return null;

            Wave actualWave = _levelGenerator.GetLevels(_levelsType)[_actualLvl].waves[_actualWaveIndex];
            _badChoice = false;
            _ballonsSpawner.WaveSpawn(actualWave);
            _trueContentType = actualWave.trueType;

            ToVoiceContent();

            yield return null;
        }

        //Проверка условия перехода на следующий уровень
        private void CheckLvl()
        {
            int endLvlScore = _levelGenerator.GetLevels(_levelsType)[_actualLvl].redyLevelScores;

            if (_lvlScore >= endLvlScore)
            {
                _gameStop = true;
                _gameOverScreen.ShowGameOverContent(_lvlScore);
                _actualLvl++;

                if (_levelGenerator.GetLevels(_levelsType).Length <= _actualLvl) _actualLvl = 0;

                _lvlScore = 0;
                endLvlScore = _levelGenerator.GetLevels(_levelsType)[_actualLvl].redyLevelScores;
                _progressController.NewWaveDataSet(endLvlScore);
            }
        }

        private void ToVoiceContent()
        {
            SoundController.PlayBallonColorSoundAction(_trueContentType);
            Debug.Log(_trueContentType);
        }

        private void PlayGameSound(SoundType soundType, bool loop) => SoundController.PlayGameSoundAction(soundType, loop);

        private void GamePaused()
        {
            SoundController.MuteSoundAction(SoundType.GameBackgroundSound, true);
            _gameStop = true;
        }

        private void GameResume()
        {
            SoundController.MuteSoundAction(SoundType.GameBackgroundSound, false);
            _gameStop = false;
        }

        private void Exit() => Application.Quit();

        private void OnEnable()
        {
            ExitGameAction += Exit;
            PauseGameAction += GamePaused;
            ResumeGameAction += GameResume;
            StartGameAction += GameStart;
            BallonTouchAction += PlayerChangeBallon;
        }

        private void OnDisable()
        {
            ExitGameAction -= Exit;
            PauseGameAction -= GamePaused;
            ResumeGameAction -= GameResume;
            StartGameAction -= GameStart;
            BallonTouchAction -= PlayerChangeBallon;
        }
    }
}
