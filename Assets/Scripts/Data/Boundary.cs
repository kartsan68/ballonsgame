﻿using UnityEngine;

namespace BallonGame.Data
{
    [System.Serializable]
    public struct Boundary
    {
        public Vector2 minBoundary;
        public Vector2 maxBoundary;
    }
}
