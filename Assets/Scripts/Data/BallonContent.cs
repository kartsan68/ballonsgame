﻿using UnityEngine;

namespace BallonGame.Data
{
    [System.Serializable]
    public struct BallonContent
    {
        //Виды изображений шара (выбираются случайным образом)
        public Sprite[] ballonSprites;
        //Изображение на шаре, тут не используется, но как мне сообщили, что пригодится в будущем
        public Sprite spriteOnBallon;
        public BallonContentType contentType;
    }
}
