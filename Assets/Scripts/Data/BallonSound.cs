﻿using UnityEngine;

namespace BallonGame.Data
{
    [System.Serializable]
    public struct BallonSound
    {
        public AudioClip audioClip;
        public BallonContentType type;
    }
}
