namespace BallonGame.Data
{
    [System.Serializable]
    public struct TapAnimationData
    {
        public float scaleX;
        public float scaleY;
        public float pressInterval;
        public float backInterval;
    }
}
