﻿namespace BallonGame.Data
{
    [System.Serializable]
    public struct Level
    {
        //Волны в уровне
        public Wave[] waves;
        //Количество очков для завершения уровня
        public int redyLevelScores;
    }
}
