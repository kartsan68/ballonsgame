﻿using UnityEngine;

namespace BallonGame.Data
{
    [System.Serializable]
    public struct Sound
    {
        public AudioClip audioClip;
        public SoundType type;
    }
}