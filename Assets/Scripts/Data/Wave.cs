﻿namespace BallonGame.Data
{
    [System.Serializable]
    public struct Wave
    {
        //Типы шаров в волне
        public BallonContentType[] waveContent;
        //Тип правильного ответа
        public BallonContentType trueType;
    }
}
